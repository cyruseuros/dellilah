package scheduling

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"time"

	"github.com/go-chat-bot/bot"
)

// ScheduleObject is an object that represents a way of scheduling
// Names is a list of names or channels to send a message to
// Message is the text for intended recipients
// TimeSignature is a chrono expression describing when the message should be sent "0/5 * * * * *" - every 5 seconds
type ScheduleObject struct {
	Name          string
	Message       string
	TimeSignature string
}

var (
	globalBool = true //Here now only to demonstrate opportunity of using closure in periodic function
	pathToJSON = "./resources/schedule.json"
)

// generateTestObj generates a scheduleObject for general testing
func generateTestObj() ScheduleObject {
	return ScheduleObject{Name: "UMEA178LX", Message: "Hello brader", TimeSignature: "0/5 * * * * *"}
}

// gaveScheduleObject saves schedule object for testing purpoes into JSON file
func saveScheduleObject(toSave ScheduleObject) {
	file, _ := json.MarshalIndent(toSave, "", " ")
	ioutil.WriteFile(pathToJSON, file, 0644)
}

// readScheduleObject reads ScheduleObject from json file that was saved with saveScheduleObject
func readScheduleObject() ScheduleObject {
	jsonFile, _ := os.Open(pathToJSON)
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var retVal ScheduleObject
	json.Unmarshal(byteValue, &retVal)
	return retVal
}

// periodic is an example of a function that is called at appropriate time intervals
// arguments can be passed through closure, or as global vars, but return value should always be -> []bot.CmdResult, error
func periodic(message string, name string) ([]bot.CmdResult, error) {
	result := bot.CmdResult{Channel: name, Message: message}
	if !globalBool {
		result.Message = " Things have changed"
	}
	return []bot.CmdResult{result}, nil

}

// flips globalBool variable to test periodic method reacting to the change
func flipGlobalBoolAsync() {
	go func() {
		time.Sleep(15 * time.Second)
		globalBool = false
	}()
}

func init() {
	saveScheduleObject(generateTestObj())
	var scheduleObject = readScheduleObject()
	config := bot.PeriodicConfig{
		CronSpec: scheduleObject.TimeSignature,
		CmdFuncV2: func() ([]bot.CmdResult, error) {
			return periodic(scheduleObject.Message, scheduleObject.Name)
		},
	}
	bot.RegisterPeriodicCommandV2("ultimate_periodic", config)
	flipGlobalBoolAsync()
}
