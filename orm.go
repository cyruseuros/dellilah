package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

const sqlDialect = "postgres"

var api = slack.New(SlackToken)

// GetCurrentSlackUsers from API
func GetCurrentSlackUsers() []slack.User {
	var users []slack.User

	users, err := api.GetUsers()
	if err != nil {
		panicMsg := "Pinging API for Slack users failed!"
		log.Panic(panicMsg)
		panic(panicMsg)
	}

	return users
}

// SlackUser is just a slack.User and gorm.Model composition
type SlackUser struct {
	gorm.Model
	slack.User
}

// GetDbSlackUsers fetches already registered users
// func GetDbSlackUsers() []SlackUser {
// 	// TODO
// }

// SaveSlackUSers migrates current Slack users to the Database
func SaveSlackUSers(append bool) {
	// TODO
	db, err := gorm.Open(sqlDialect, DatabaseURL)
	if err != nil {
		panic("Failed to connect to database.")
	}
	defer db.Close()

	for _, user := range GetCurrentSlackUsers() {
		if append {
			db.FirstOrCreate(&user)
		} else {
			db.Create(&user)
		}
	}
}

// AutoMigrate sets up the ORM in our DB
func AutoMigrate() {
	db, err := gorm.Open(sqlDialect, DatabaseURL)
	if err != nil {
		panic("Failed to connect to database.")
	}
	defer db.Close()

	db.AutoMigrate(&SlackUser{})
}
