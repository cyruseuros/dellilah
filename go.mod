module gitlab.com/jjzmajic/dellilah

go 1.12

require (
	github.com/go-chat-bot/bot v0.0.0-20190725143134-34d7e9b3b150
	github.com/go-chat-bot/plugins v0.0.0-20190725144559-b3a4c848665a
	github.com/jinzhu/gorm v1.9.10
	github.com/lusis/go-slackbot v0.0.0-20180109053408-401027ccfef5 // indirect
	github.com/lusis/slack-test v0.0.0-20190426140909-c40012f20018 // indirect
	github.com/nlopes/slack v0.5.0
	github.com/sirupsen/logrus v1.2.0
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/stretchr/testify v1.3.0 // indirect
)
