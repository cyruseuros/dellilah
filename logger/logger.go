package logger

import (
	"fmt"
	"os"
	"time"

	"github.com/go-chat-bot/bot"
)

var (
	messages = make(map[string]*[]string)
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func logInstant(command *bot.PassiveCmd) (string, error) {
	f, err := os.OpenFile(fmt.Sprintf(".\\logs\\logs%s.txt", command.Channel), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	check(err)
	defer f.Close()
	dt := time.Now()
	logMessage := fmt.Sprintf("%v %s: %s\n", dt, command.User.Nick, command.Raw)
	_, err = f.WriteString(logMessage)
	check(err)
	return "", nil
}

func logBuffer(command *bot.PassiveCmd) (string, error) {
	dt := time.Now()
	logMessage := fmt.Sprintf("%v %s: %s\n", dt, command.User.Nick, command.Raw)
	channelMessagesAdr, exists := messages[command.Channel]
	var channelMessages []string
	if exists {
		channelMessages = *channelMessagesAdr
		channelMessages = append(channelMessages, logMessage)
	} else {
		channelMessages = []string{logMessage}
	}
	messages[command.Channel] = &channelMessages
	if len(channelMessages) >= 10 {
		f, err := os.OpenFile(fmt.Sprintf(".\\logs\\logs%s.txt", command.Channel), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		check(err)
		defer f.Close()
		for _, logMessage := range channelMessages {
			_, err = f.WriteString(logMessage)
			check(err)
		}
		channelMessages = channelMessages[:0] //deleting messages after logging
	}
	return "", nil
}

func init() {
	bot.RegisterPassiveCommand(
		"logger",
		logBuffer)
}
