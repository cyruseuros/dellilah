package main

import (
	"os"
)

// SlackToken stores slack auth
var SlackToken = os.Getenv("SLACK_TOKEN")
// DatabaseURL stores conn string
var DatabaseURL = os.Getenv("DATABSE_URL")
